/*:
 # Let's Combine
 ### Presented @cocoa CPH event December 19, 2919 by Per Friis
 
 ## Table Of Content
 * [Just get the Data](Just%20get%20the%20data)
 * [Mapping the output](Map%20the%20output)
 * [Check for http errors](Check%20for%20http%20error)
 * [Parse the output to an object](Parse%20the%20output%20to%20an%20object)
 * [Simplify the publisher](Simplify%20the%20Publisher)
 * [Tricker the next](tricker%20the%20next)
 * [Wrap async calls](Wrap%20async%20calls)

 This is the playground that I used for the demo, if you have any question or comments you are welcome to contact me!
 you can find me on [LinkedIn](https://www.linkedin.com/in/friisconsult/) or [twitter@MacFriis](https://twitter.com/MacFriis)
    
 
 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
 */
