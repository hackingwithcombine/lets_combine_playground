//: [Previous](@previous)
/*:
 # How simple can it be
 Here is how simple we can do a REST API call and get the data returned.
 
 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
*/

import Foundation
import Combine

/// Valid URL that returns a random Chuck Norris joke
let chuckNorrisJokeURL = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL but invalid URI (not found page)
let notFoundURL = URL(string: "https://api.chucknorris.io/handle")!

/// The publisher
let restAPI = URLSession.shared.dataTaskPublisher(for: chuckNorrisJokeURL)
    .map(\.data)
 

/// Subscribe to the publisher
let cancellable = restAPI.sink(receiveCompletion: { completion in
    print("-------------------------------------")
    print("Sink received completed:\(completion)")
    print("-------------------------------------")
}) { data in
    print("The data converted to string returned:\(String(data:data,encoding: .utf8)!)")
}

//: [Back to overview](Overview)

//: [Next](@next)
