//: [Previous](@previous)
/*:
 # Wrap asynchronous
 Here we wrap and asynchronous call in to a publisher
 
 - note: Yes we could expect  to implement this as a publisher just like the `dataTaskPublisher<(data,URLResponse),Error` but as we don't know the roadmap, here is a way to do it
 out self.

 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
 */
import Foundation
import Combine
import MapKit
import CoreLocation
enum GeoCoderMyError: Error {
    case noMatchesFound
}

let 🍏 = "1 infinity loop, Cupertino, ca, us"
let 🚮 = "frøslevvej,  København Ø"
let 🏡 = "64 Frøslevvej, 2610 Rødovre"

let subscription = Future<[CLPlacemark], Error>{ promise in
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(🍏) { (placeMarks, error) in
        guard error == nil else {
            promise(.failure(error!))
            return
        }
        guard let placeMarks = placeMarks else {
            promise(.failure(GeoCoderMyError.noMatchesFound))
            return
        }
            promise(.success(placeMarks))
        }
        }
    .tryMap({ (placemarks) -> CLPlacemark in
        guard let placemark = placemarks.first else {
            throw GeoCoderMyError.noMatchesFound
        }
        return placemark
        
    })
    .tryMap({(placemark) -> MKPlacemark in MKPlacemark(placemark: placemark)})
    .sink(receiveCompletion: { completion in
           print("-------------------------------------")
           print("Sink received completed:\(completion)")
           print("-------------------------------------")
       }) { placeMarks in
           print(placeMarks)
       }


//: [Back to overview](Overview)

//: [Next](@next)
