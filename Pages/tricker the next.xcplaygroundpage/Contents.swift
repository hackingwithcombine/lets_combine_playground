//: [Previous](@previous)
/*:
 # Trick the next
 Here we uses another publisher, within the subscription of the first
 
 - note: I'm not self completely confident that this is the best way of doing this, however it is a way to
 make the example here in PlayGround.
 
 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
*/

import Foundation
import Combine
import AppKit

/// ChuckNorrisJoke
/// - Description: This is a struct created using QuickType.io tool, that let you copy a json string and past
///     it as a Codable struct, you can use directly.
///
///     I have cleaned this on, to just use the struct  and added the types that are relevante
struct ChuckNorrisJoke: Codable {
    let categories: [String]
    let createdAt : String
    let iconURL : String
    let id : String
    let updatedAt: String
    let url : String
    let value: String

    enum CodingKeys: String, CodingKey {
        case categories
        case createdAt = "created_at"
        case iconURL = "icon_url"
        case id
        case updatedAt = "updated_at"
        case url, value
    }
}

class SomeClass: ObservableObject {
    @Published var someUrl : URL = URL(string: "https://")!
    var image:NSImage!
}

/// We need an error type to return
enum HttpResponseFailure:Error {
    case invalidServerResponse(_ statusCode : Int)
}

var subscribers : [AnyCancellable] = []

/// Valid URL that returns a random Chuck Norris joke
let chuckNorrisJokeURL = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL but invalid URI (not found page)
let notFoundURL = URL(string: "https://api.chucknorris.io/jokes/nojoke")!


/// The publisher
func getChuckNorrisJoke() -> AnyPublisher<ChuckNorrisJoke,Error> {
    URLSession.shared.dataTaskPublisher(for: chuckNorrisJokeURL)
    .tryMap({data,response -> Data in
        let httpResponse = response as! HTTPURLResponse
        guard httpResponse.statusCode == 200 else {
            throw HttpResponseFailure.invalidServerResponse(httpResponse.statusCode)
        }
        return data
    })
    .decode(type: ChuckNorrisJoke.self, decoder: JSONDecoder())
    .eraseToAnyPublisher()
}


func getImage(url: URL) -> AnyPublisher<NSImage,Error> {
        URLSession.shared.dataTaskPublisher(for: url)
            .map(\.data)
            .tryMap ({ (data) -> NSImage in NSImage(data: data)!})
            .eraseToAnyPublisher()
}

var iconImage: NSImage!
/// Subscribe to the publisher
let cancellable = getChuckNorrisJoke().sink(receiveCompletion: { completion in
    print("-------------------------------------")
    print("Sink received completed:\(completion)")
    print("-------------------------------------")
}) { chuckNorrisJoke in
    print("\(chuckNorrisJoke.value)")
    if let iconUrl = URL(string: chuckNorrisJoke.iconURL) {
        
        getImage(url: iconUrl)
            .sink(receiveCompletion: { completion in
            print("-------------------------------------")
            print("Sink received completed:\(completion)")
            print("-------------------------------------")
        }) { image in
            iconImage = image
        }
        .store(in: &subscribers)
    }
}

//: [Back to overview](Overview)

//: [Next](@next)
