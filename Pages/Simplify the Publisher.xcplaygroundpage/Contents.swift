//: [Previous](@previous)

/*:
 # Say what!
 `Publishers.Decode<Publishers.TryMap<URLSession.DataTaskPublisher, JSONDecoder.Input>, ChuckNorrisJoke, JSONDecoder>`
 
 is the type of the restAPI publisher, as each time we add a new operation in the chain, the publisher type just expands. it is not a big problem as we only have to
 focus on the last publishers type, how ever it can be some what confusion to look at types like this, and if we uses the publisher as a return value in a _func_, we can't write such a type in the function declaration.
 
  has provided a way to cook it down to something more manageable, in this case `AnyPublisher<ChuckNorrisJoke, Error>` using .`eraseToAnyPublisher`

 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
*/

import Foundation
import Combine


/// ChuckNorrisJoke
/// - Description: This is a struct created using QuickType.io tool, that let you copy a json string and past
///     it as a Codable struct, you can use directly.
///
///     I have cleaned this on, to just use the struct  and added the types that are relevante
struct ChuckNorrisJoke: Codable {
    let categories: [String]
    let createdAt : String
    let iconURL : String
    let id : String
    let updatedAt: String
    let url : String
    let value: String

    enum CodingKeys: String, CodingKey {
        case categories
        case createdAt = "created_at"
        case iconURL = "icon_url"
        case id
        case updatedAt = "updated_at"
        case url, value
    }
}

/// We need an error type to return
enum HttpResponseFailure:Error {
    case invalidServerResponse(_ statusCode : Int)
}

/// Valid URL that returns a random Chuck Norris joke
let chuckNorrisJokeURL = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL but invalid URI (not found page)
let notFoundURL = URL(string: "https://api.chucknorris.io/jokes/nojoke")!


/// The publisher
let restAPI = URLSession.shared.dataTaskPublisher(for: chuckNorrisJokeURL)
    .tryMap({data,response -> Data in
        let httpResponse = response as! HTTPURLResponse
        guard httpResponse.statusCode == 200 else {
            throw HttpResponseFailure.invalidServerResponse(httpResponse.statusCode)
        }
        return data
    })
    .decode(type: ChuckNorrisJoke.self, decoder: JSONDecoder())
    .eraseToAnyPublisher()



/// Subscribe to the publisher
let cancellable = restAPI.sink(receiveCompletion: { completion in
    print("-------------------------------------")
    print("Sink received completed:\(completion)")
    print("-------------------------------------")
}) { chuckNorrisJoke in
    print("\(chuckNorrisJoke.value)")    
}

//: [Back to overview](Overview)

//: [Next](@next)
