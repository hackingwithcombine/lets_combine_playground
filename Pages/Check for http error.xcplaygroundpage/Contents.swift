//: [Previous](@previous)
/*:
 # Not always the golden path
 up to now we have assumed that everything is working well, no error handling. And we can safely do that, as the app won't crash if the REST API don't respond successfully or for some other reason we don't get our data. as the publisher "just" finish with a completion = failure
 
 All that "happens" is the nothing happens, we don't get a bucket of bytes, but otherwise, it just runs
 
 Well in some cases, it just might be reasonable to asses the errors that are returned to us, maybe we have a user to inform, or we have to make the app to take another path. so lets do some error handling
 
 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
*/

import Foundation
import Combine

/// We need an error type to return
enum HttpResponseFailure:Error {
    case invalidServerResponse(_ statusCode : Int)
}

/// Valid URL that returns a random Chuck Norris joke
let chuckNorrisJokeURL = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL but invalid URI (not found page)
let notFoundURL = URL(string: "https://api.chucknorris.io/jokes/nojoke")!

/// The publisher
let restAPI = URLSession.shared.dataTaskPublisher(for: chuckNorrisJokeURL)
    .tryMap({data,response -> Data in
        let httpResponse = response as! HTTPURLResponse
        guard httpResponse.statusCode == 200 else {
            throw HttpResponseFailure.invalidServerResponse(httpResponse.statusCode)
        }
        return data
    })
    
/// Subscribe to the publisher
let cancellable = restAPI.sink(receiveCompletion: { completion in
    print("-------------------------------------")
    print("Sink received completed:\(completion)")
    print("-------------------------------------")
}) { data in
    print("The data converted to string returned:\(String(data:data,encoding: .utf8)!)")
}
//: [Back to overview](Overview)

//: [Next](@next)
