//: [Previous](@previous)
/*:
 # Parsing JSON!
 We all love JSON, well I don't know about you, but I do. And I get it everywhere. I even provide it my self, every time I make a backend,
 it's a REST API returning Json.
 
 With iOS 9,  Introduce the `Codable` protocol, enabling you to parse JSON to and from a Struct or Class. It saved me a lot of time.
 
 To make a good thing even better I found a tool called [Quick Type](https://app.quicktype.io) where you can pars a json string, and get the Swift code to pars it in to a struct. and from a struct to a Json string.
 __Yes it also supports ObjectiveC and other language__
 
 There is also a plugin to XCode, but i lacks the flexibility of online-tool version at this point (Dec. 2019)
 
 ![Friis Consult ApS](logo.png "Friis Consult Aps, Friis Mobility ApS")  Copyright: © 2019 Friis Mobility ApS / Friis Consult ApS
*/

import Foundation
import Combine


/// ChuckNorrisJoke
/// - Description: This is a struct created using QuickType.io tool, that let you copy a json string and past
///     it as a Codable struct, you can use directly.
///
///     I have cleaned this on, to just use the struct  and added the types that are relevante
struct ChuckNorrisJoke: Codable {
    let categories: [String]
    let createdAt : String
    let iconURL : String
    let id : String
    let updatedAt: String
    let url : String
    let value: String
//    let dummy : String

    enum CodingKeys: String, CodingKey {
        case categories
        case createdAt = "created_at"
        case iconURL = "icon_url"
        case id
        case updatedAt = "updated_at"
        case url, value
//        case dummy
    }
}

/// We need an error type to return
enum HttpResponseFailure:Error {
    case invalidServerResponse(_ statusCode : Int)
}

/// The URL to call
let url = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL that returns a random Chuck Norris joke
let chuckNorrisJokeURL = URL(string: "https://api.chucknorris.io/jokes/random")!

/// Valid URL but invalid URI (not found page)
let notFoundURL = URL(string: "https://api.chucknorris.io/jokes/nojoke")!


/// The publisher
let restAPI = URLSession.shared.dataTaskPublisher(for: chuckNorrisJokeURL)
    .tryMap({data,response -> Data in
        let httpResponse = response as! HTTPURLResponse
        guard httpResponse.statusCode == 200 else {
            throw HttpResponseFailure.invalidServerResponse(httpResponse.statusCode)
        }
        return data
    })
    .decode(type: ChuckNorrisJoke.self, decoder: JSONDecoder())
    



/// Subscribe to the publisher
let cancellable = restAPI.sink(receiveCompletion: { completion in
    print("-------------------------------------")
    print("Sink received completed:\(completion)")
    print("-------------------------------------")
}) { chuckNorrisJoke in
    print("\(chuckNorrisJoke.value)")
}

//: [Back to overview](Overview)

//: [Next](@next)
